const express = require('express');
const { Server } = require('socket.io');
const app = express();
const http = require('http');
const server = http.createServer(app);
const io = new Server(server)

app.get('/', (req, res) => {
  res.status(200).type('html').send('<h1>Hello world</h1>');
});

io.on('connection',(socket) => {
    console.log('connected',socket.id);

    socket.on('message:send',({ message }) => {
      console.log('message:send : ',message);
      io.emit('message:send',{ message, socketId: socket.id })
    })

    socket.on('disconnect',() => {
      console.log('disconnect');
    })
})

server.listen(5000, () => {
  console.log('listening on *:5000');
});

