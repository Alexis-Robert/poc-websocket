import { useEffect, useState } from "react";
import { io } from "socket.io-client";

// const socket = io('http://localhost:5000',{ transports : ['websocket'] })
const socket = io('https://poc-websocket.onrender.com',{ transports : ['websocket'] })

function App() {
  const [clearInputOnSend, setClearInputOnSend] = useState(false);
  const [message, setMessage] = useState('');
  const [chat, setChat] = useState([]);

  const handleSendMessage = (e) => {
    e.preventDefault();

    if (clearInputOnSend) {
      setMessage('')
    }

    socket.emit('message:send', { message })
  }

  useEffect(() => {
    socket.on('message:send',({ message, socketId }) => {
      setChat(old => [...old, { message, socketId }])
    })
  },[])

  return (
    <div className="chat">
      <ul>
        {
          chat.map((item,index) => {
            return <li key={index}>
                      <strong>{item.socketId}</strong>
                      <p>
                      {item.message}
                      </p>
                   </li>
          })
        }
      </ul>
      <form onSubmit={handleSendMessage}>
        <label>
          <input type="checkbox" checked={clearInputOnSend} onChange={e => setClearInputOnSend(e.target.checked)}/>
          clear input on send
        </label>
        <input type="text" value={message} onChange={e => setMessage(e.target.value)} />
        <button type="submit">Send</button>
      </form>
    </div>
  );
}

export default App;
